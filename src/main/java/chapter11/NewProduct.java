package chapter11;

public interface NewProduct {

    String getDate();
    void setDate(String date);
}
